package ru.kazakov.iteco.util;

import ru.kazakov.iteco.enumeration.TaskManagerCommand;
import ru.kazakov.iteco.view.ConsoleWriter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleReader {

    public BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public String enterIgnoreEmpty() throws IOException {
        String name = reader.readLine();
        Format format = new Format();
        while (format.isEmpty(name)) {
            name = reader.readLine();
        }
        return name;
    }

    public String read() throws IOException {
        StringBuilder builder = new StringBuilder().append("");
        String temp = reader.readLine();
        while (!temp.equals(TaskManagerCommand.SAVE.getValue())) {
            builder.append(temp);
            builder.append(new ConsoleWriter().separator);
            temp = reader.readLine();
        }
        return builder.toString();
    }

    public String read(TaskManagerCommand endCommand) throws IOException {
        StringBuilder builder = new StringBuilder();
        String temp = "";
        while (!temp.equals(endCommand.getValue())) {
            builder.append(temp);
            temp = reader.readLine();
        }
        return builder.toString();
    }

}
