package ru.kazakov.iteco.view;

import static ru.kazakov.iteco.enumeration.TaskManagerCommand.*;
import ru.kazakov.iteco.enumeration.Action;
import ru.kazakov.iteco.enumeration.EntityType;
import ru.kazakov.iteco.util.ConsoleReader;
import ru.kazakov.iteco.util.Format;
import java.io.IOException;

public class ConsoleWriter {

    public final String separator = System.lineSeparator();

    private final Format format = new Format();

    public String enterEntityName(EntityType entityType) throws IOException {
        showEnterName(entityType);
        ConsoleReader consoleReader = new ConsoleReader();
        String result = consoleReader.enterIgnoreEmpty();
        return result;
    }

    public void showEnterName(EntityType entityType) {
        System.out.println("ENTER " + entityType.getValue().toUpperCase() + " NAME:");
    }

    public void showActionInBreakets(Action action) {
        System.out.println("[" + action + "]");
    }

    public void showNotActionInBreakets(Action action) {
        System.out.println("[NOT " + action + "]");
    }

    public void showNameInBreakets(EntityType entityType, String name) {
        System.out.println("[" + entityType.getValue().toUpperCase() + ": " + name + "]");
    }

    public void showNameIsExist(EntityType entityType) {
        System.out.println(format.firstUpperCase(entityType.getValue()) + " with this name is already exists. Use another " + entityType.getValue() + " name.");
    }

    public void showNameIsAlreadyAdded(EntityType entityType) {
        System.out.println(format.firstUpperCase(entityType.getValue()) + " is already added. Use " + PROJECT_LIST_TASKS.getValue() + " to see " + entityType.getValue() +
                "s in project.");
    }

    public void showNameNotExist(EntityType entityType) {
        System.out.println(format.firstUpperCase(entityType.getValue()) + " with this name doesn't exist. Use \"" + entityType.getValue() +  "-list\" to show all projects.");
    }

    public void showSuccessfullyDone(EntityType entityType, Action action) {
        System.out.println(format.firstUpperCase(entityType.getValue()) + " successfully " + action.toString().toLowerCase() + "!");
    }

    public void showInfoIsEmpty(EntityType entityType) {
        System.out.println(format.firstUpperCase(entityType.getValue()) + " is empty. Use \"" + entityType.getValue() +  "-update\" to update this project.");
    }

    public void showEnterInformation(EntityType entityType) {
        System.out.println("ENTER " + entityType.getValue().toUpperCase() + " INFORMATION");
    }

    public void showFinishAndSave() {
        System.out.println("Use \"" + SAVE.getValue() + "\" to finish entering, and save information.");
    }

    public void showListIsEmpty(EntityType entityType) {
        System.out.println(format.firstUpperCase(entityType.getValue()) + " list is empty. Use \"" + entityType.getValue() +  "-create\" to create " + entityType.getValue() +  ".");
    }

    public void showTaskListIsEmpty() {
        System.out.println("The project has no tasks. Use " + PROJECT_ADD_TASK.getValue() + " to add task to project.");
    }

    public void showListInBreakets(EntityType entityType) {
        System.out.println("[" + entityType.getValue().toUpperCase() + "S LIST]");
    }

    public void showClear(EntityType entityType) {
        System.out.println("[ALL " + entityType.getValue().toUpperCase() + "S REMOVED]" + separator + "" + format.firstUpperCase(entityType.getValue()) +
                "s successfully removed!" + separator);
    }

    public void showCommandNotExist() {
    System.out.println("Command doesn't exist. Use \"" + HELP.getValue() + "\" to show all commands." + separator);
    }

    public void showHelp() {
        System.out.println(
               HELP.getValue() + ": Show all commands." + separator +
               PROJECT_CREATE.getValue() + ": Create new project." + separator +
               PROJECT_GET.getValue() + ": Show all project information." + separator +
               PROJECT_UPDATE.getValue() + ": Update project information." + separator +
               PROJECT_ADD_TASK.getValue() + ": Add task to project." + separator +
               PROJECT_LIST_TASKS.getValue() + ": Show all tasks in project." + separator +
               PROJECT_REMOVE.getValue() + ": Remove project." + separator +
               PROJECT_LIST.getValue() +  ": Show all projects." + separator +
               PROJECT_CLEAR.getValue() +   ": Remove all projects." + separator +
               TASK_CREATE.getValue() +   ": Create new task." + separator +
               TASK_GET.getValue() +   ": Show all task information." + separator +
               TASK_UPDATE.getValue() +   ": Update task information." + separator +
               TASK_REMOVE.getValue() +   ": Remove task." + separator +
               TASK_LIST.getValue() +   ": Show all tasks." + separator +
               TASK_CLEAR.getValue() +  ": Remove all tasks." + separator +
               SAVE.getValue() +   ": Save all entered information to project or task." + separator +
               EXIT.getValue() +    ": Close task manager.");
               separateLines();
    }

    public void showManagerOpened() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    public void showManagerClosed() {
        System.out.println("*** MANAGER CLOSED ***");
    }

    public void printListValue(int counter, String value) {
        System.out.println(counter + ". " + value);
    }

    public void printInformation(String information) {
        System.out.println(information);
    }

    public void separateLines() {
        System.out.print(separator);
    }

}
