package ru.kazakov.iteco.enumeration;

public enum TaskManagerCommand {

    HELP ("help"),
    PROJECT_CREATE ("project-create"),
    PROJECT_GET ("project-get"),
    PROJECT_UPDATE ("project-update"),
    PROJECT_ADD_TASK ("project-add-task"),
    PROJECT_LIST_TASKS ("project-list-tasks"),
    PROJECT_REMOVE ("project-remove"),
    PROJECT_LIST ("project-list"),
    PROJECT_CLEAR ("project-clear"),
    TASK_CREATE ("task-create"),
    TASK_GET ("task-get" ),
    TASK_UPDATE ("task-update"),
    TASK_REMOVE ("task-remove"),
    TASK_LIST ("task-list"),
    TASK_CLEAR ("task-clear"),
    SAVE("-save"),
    EXIT ("exit"),
    NONE("");

    private String value = "";

    public String getValue() {
        return value;
    }

    TaskManagerCommand(String value) {
        this.value = value;
    }

}
