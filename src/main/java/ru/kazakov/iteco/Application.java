package ru.kazakov.iteco;
import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.view.ConsoleWriter;
import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {
        new ConsoleWriter().showManagerOpened();
        new Bootstrap().init();
    }

}
