package ru.kazakov.iteco.service;

import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.repository.TaskRepository;

import java.util.List;

public class TaskService extends AbstractService<Task> {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public String getName(String id) {
        return taskRepository.getName(id);
    }

    public void setName(String name, String id) {
        taskRepository.setName(name, id);
    }

    public String getProject(String id) {
        return taskRepository.getProject(id);
    }

    public void setProject(String id, String projectId) {
        taskRepository.setProject(id, projectId);
    }

    @Override
    public void merge(Task entity) {
        taskRepository.merge(entity);
    }

    @Override
    public void persist(Task entity) {
        taskRepository.persist(entity);
    }

    public void persist(String name) {
        Task task = new Task(name);
        taskRepository.persist(task);
    }

    @Override
    public void remove(String id) {
        taskRepository.remove(id);
    }

    public void remove(List<String> ids) {
        taskRepository.remove(ids);
    }

    @Override
    public void removeAll() {
        taskRepository.removeAll();
    }

    @Override
    public Task findOne(String id) {
        return taskRepository.findOne(id);
    }

    public Task findOne(String name, boolean isByName) {
        List<Task> list = taskRepository.findAll();
        return list.stream()
                .filter(v -> v.getName().equals(name))
                .findFirst().orElse(null);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public List<Task> findAll(List<String> ids) {
        return taskRepository.findAll(ids);
    }

    @Override
    public boolean isEmpty() {
        return taskRepository.isEmpty();
    }

    public boolean isEmpty(String id) {
        return taskRepository.isEmpty(id);
    }

    public boolean contains(String name) {
        List<Task> list = taskRepository.findAll();
        return list.stream()
                .anyMatch(v -> v.getName().equals(name));
    }

}
