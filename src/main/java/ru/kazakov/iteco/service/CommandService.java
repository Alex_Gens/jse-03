package ru.kazakov.iteco.service;

import ru.kazakov.iteco.enumeration.TaskManagerCommand;
import java.util.Arrays;
import java.util.List;

public class CommandService {

    public TaskManagerCommand getCommand (String entered) {
        List<TaskManagerCommand> commands = Arrays.asList(TaskManagerCommand.values());
        return commands.stream()
                .filter(com -> entered.equals(com.getValue()))
                .findFirst().orElse(TaskManagerCommand.NONE);
    }

}
