package ru.kazakov.iteco.service;

import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.repository.ProjectRepository;
import ru.kazakov.iteco.repository.TaskRepository;
import java.util.List;

public class ProjectService extends AbstractService<Project> {

    private TaskRepository taskRepository;
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public String getName(String id) {
        return projectRepository.getName(id);
    }

    public void setName(String name, String id) {
        projectRepository.setName(name, id);
    }

    @Override
    public void merge(Project entity) {
        projectRepository.merge(entity);
    }

    @Override
    public void persist(Project entity) {
        projectRepository.persist(entity);
    }

    public void persist(String name) {
        Project project = new Project(name);
        projectRepository.persist(project);
    }

    @Override
    public void remove(String id) {
        projectRepository.remove(id);
    }

    public void remove(List<String> ids) {
        projectRepository.remove(ids);
    }

    @Override
    public void removeAll() {
        projectRepository.removeAll();
    }

    @Override
    public Project findOne(String id) {
        return projectRepository.findOne(id);
    }

    public Project findOne(String name, boolean isByName) {
        List<Project> list = projectRepository.findAll();
        return list.stream()
                .filter(v -> v.getName().equals(name))
                .findFirst().orElse(null);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAll(List<String> ids) {
        return projectRepository.findAll(ids);
    }

    @Override
    public boolean isEmpty() {
        return projectRepository.isEmpty();
    }

    public boolean isEmpty(String id) {
        return projectRepository.isEmpty(id);
    }

    public boolean contains(String name) {
        List<Project> list = projectRepository.findAll();
        return list.stream()
                .anyMatch(v -> v.getName().equals(name));
    }

}
